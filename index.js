// Import required packages and modules
const express = require("express"); // Express.js for building the API
const mongoose = require("mongoose"); // Mongoose for MongoDB interactions
const cors = require("cors"); // CORS for handling cross-origin requests
const userRoutes = require("./routes/userRoutes"); // User routes
const productRoutes = require("./routes/productRoutes"); // Product routes

const port = 4040; // Port to run the server on (default: 4000)
const app = express(); // Create an Express application

// Middleware setup
app.use(cors()); // Enable CORS for cross-origin requests
app.use(express.json()); // Parse JSON requests
app.use(express.urlencoded({ extended: true })); // Parse URL-encoded requests


// Connect to MongoDB using Mongoose
mongoose.connect(
    "mongodb+srv://simalecjericho:admin123@course-booking.zpjwlru.mongodb.net/MERN?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

// Once MongoDB connection is open, log a message
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

// Define routes
app.use("/users", userRoutes); // User-related routes
app.use("/product", productRoutes); // Product-related routes

// Start the Express server and listen on the specified port
if (require.main === module) {
    app.listen(process.env.PORT || port, () => {
        console.log(`API is now online on port ${process.env.PORT || port}`);
    });
}

// Export the Express app and Mongoose for testing purposes
module.exports = app;
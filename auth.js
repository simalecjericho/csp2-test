const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI"; // Secret key for JWT

// Function to create an access token for a user
module.exports.createAccessToken = (user) => {
    // Create a data object with user details
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };

    // Sign the data object using the secret key and return the token
    return jwt.sign(data, secret, {});
};

// Middleware to verify the user's access token
module.exports.verify = (req, res, next) => {
    // Retrieve the access token from the request headers
    let token = req.headers.authorization;

    // Check if the token is undefined
    if (typeof token === "undefined") {
        return res.send({ auth: "Failed, No Token" }); // No token provided, authentication failed
    } else {
        token = token.slice(7, token.length); // Remove the "Bearer " prefix from the token

        // Verify the token using the secret key and handle errors
        jwt.verify(token, secret, function (err, decodedToken) {
            if (err) {
                return res.send({
                    auth: "Failed",
                    message: err.message
                }); // Token verification failed, authentication failed
            } else {
                req.user = decodedToken; // Store the decoded user data in the request object
                next(); // Move on to the next middleware or route
            }
        });
    }
};

// Middleware to verify if the user is an admin
module.exports.verifyAdmin = (req, res, next) => {
    if (req.user.isAdmin) {
        next(); // User is an admin, proceed to the next middleware or route
    } else {
        return res.status(400).send({
            auth: "Failed",
            message: "Action Forbidden"
        }); // User is not an admin, authentication failed
    }
};
const mongoose = require("mongoose");

// Define the Mongoose schema for the 'User' model
const userSchema = new mongoose.Schema({
    email: { type: String, required: true }, // User's email address, it's required.
    password: { type: String, required: true }, // User's password, it's required.
    isAdmin: { type: Boolean, default: false }, // A flag indicating whether the user is an admin, default is false.
    orderedProduct: [{
        products: [{
            productId: { type: mongoose.Schema.Types.ObjectId, ref: 'Product' }, // ID of the ordered product, referring to the 'Product' model.
            productName: { type: String, required: true }, // Name of the ordered product, it's required.
            quantity: { type: Number, required: true }, // Quantity of the ordered product, it's required.
        }],
        totalAmount: { type: Number, required: true }, // Total amount for the order, it's required.
        purchasedOn: { type: Date, default: new Date() }, // Date of purchase, defaults to the current date.
    }],
    cart: [{
        product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product' }, // ID of the product in the user's cart, referring to the 'Product' model.
        quantity: { type: Number, required: true, default: 1 }, // Quantity of the product in the cart, default is 1.
        productPrice: { type: Number, required: true }, // Price of the product in the cart, it's required.
        totalAmount: { type: Number, required: true }, // Total amount for the product in the cart, it's required.
    }],
});

module.exports = mongoose.model("User", userSchema); // Export the 'User' model.
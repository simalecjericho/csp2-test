const mongoose = require("mongoose");

// Define the product schema
const productSchema = new mongoose.Schema({
    name: { type: String, required: true }, // Product name (required)
    description: { type: String, required: true }, // Product description (required)
    price: { type: Number, required: true }, // Product price (required)
    imageUrl: {
        type: String, // You can store the URL or file path here
    },
    isActive: { type: Boolean, default: true }, // Product activation status (default: true)
    createdOn: { type: Date, default: new Date() }, // Date when the product was created (default: current date)
    userOrders: [{
        userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }, // Reference to the User model
        orderId: { type: String } // Order ID associated with the product
    }]
});

// Export the Product model
module.exports = mongoose.model("Product", productSchema);
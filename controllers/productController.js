// Import required models and packages
const Product = require("../models/Product"); // Product model for interacting with product data
const User = require("../models/User");

// Function to create a new product
module.exports.createProduct = (req, res) => {
    // Extract the product details from the request body
    const { name, description, price, imageUrl } = req.body;

    // Validate product details
    if (!name || !description || !price || price <= 0) {
        // If any of the required fields is missing or price is non-positive, send an error message
        return res.send({ message: "Product creation failed, invalid input data" });
    }

    // Create a new Product object with name, description, and price
    const newProduct = new Product({
        name: name,
        description: description,
        price: price
        // imageUrl: imageUrl,
    });

    // Save the new product to the database and handle errors
    newProduct.save()
        .then(() => {
            return res.send({ message: 'Product successfully created!' });
        })
        .catch(error => {
            return res.send({ message: 'Product creation failed, internal server error' });
        });
};

// Function to retrieve all products
module.exports.getAllProducts = (req, res) => {
    // Find and return all products in the database
    return Product.find({}).then(result => {
        return res.send(result);
    });
};

// Function to retrieve all active products
module.exports.getAllActive = (req, res) => {
    // Find and return all active products in the database
    return Product.find({ isActive: true }).then(result => {
        return res.send(result);
    });
};

// Function to retrieve a specific product by ID
module.exports.getProduct = (req, res) => {
    // Retrieve the product ID from the URL parameters
    return Product.findById(req.params.productId).then(result => {
        return res.send(result);
    });
};

// Function to update a product
module.exports.updateProduct = (req, res) => {
    // Extract the updated product details from the request body
    const { name, description, price } = req.body;

    // Validate product details
    if (!name || !description || !price || price <= 0) {
        // If any of the required fields is missing or price is non-positive, send an error message
        return res.send({ message: "Product update failed, invalid input data" });
    }

    // Create an object with updated product details
    const updatedProduct = {
        name: name,
        description: description,
        price: price
    };

    // Find and update the product by its ID and handle errors
    Product.findByIdAndUpdate(req.params.productId, updatedProduct)
        .then(() => {
            return res.send({ message: 'Product successfully updated!' });
        })
        .catch(error => {
            return res.send({ message: 'Product update failed, internal server error' });
        });
};

// Function to archive a product
module.exports.archiveProduct = (req, res) => {
    // Archive a product by setting isActive to false and handle errors
    return Product.findByIdAndUpdate(req.params.productId, { isActive: false }).then((product, error) => {
        if (error) {
            return res.send({ message: ('Product archiving failed, internal server error') });
        } else {
            return res.send({ message: ('Product successfully archived!') });
        };
    });
};

// Function to activate a product
module.exports.activateProduct = (req, res) => {
    // Activate a product by setting isActive to true and handle errors
    return Product.findByIdAndUpdate(req.params.productId, { isActive: true }).then((product, error) => {
        if (error) {
            return res.send({ message: ('Product activation failed, internal server error') });
        } else {
            return res.send({ message: ('Product successfully activated!') });
        };
    });
};
// Import required models, packages, and modules
const User = require("../models/User"); // User model for interacting with user data
const Product = require("../models/Product"); // Product model for interacting with product data
const bcrypt = require('bcrypt'); // bcrypt for password hashing
const auth = require("../auth"); // Authentication middleware for token generation and verification

// Function to register a new user
module.exports.registerUser = (reqBody) => {
    return new Promise(async (resolve, reject) => {
        // Extract the password from the request body
        const { email, password } = reqBody;

        // Define regular expressions for password complexity
        const uppercaseRegex = /[A-Z]/;            // At least one uppercase letter
        const numberRegex = /[0-9]/;               // At least one digit
        const specialCharRegex = /[!@#$%^&*]/;     // At least one special character
        const minLength = 6;                       // Minimum length of 6 characters

        // Initialize an array to collect validation errors
        const errors = [];

        // Check if the password meets the complexity requirements
        if (!uppercaseRegex.test(password)) {
            errors.push('Password does not have an uppercase letter');
        }

        if (!numberRegex.test(password)) {
            errors.push('Password does not have a number');
        }

        if (!specialCharRegex.test(password)) {
            errors.push('Password does not have a special character');
        }

        if (password.length < minLength) {
            errors.push('Password is not at least 6 characters long');
        }

        // If there are validation errors, reject with the error messages
        if (errors.length > 0) {
            return reject({
                message: 'Password validation failed',
                errors: errors
            });
        }

        try {
            // Create a new User object with email and hashed password
            let newUser = new User({
                email: email,
                password: bcrypt.hashSync(password, 10)
            });

            // Save the new user to the database and handle errors
            const savedUser = await newUser.save();
            resolve({ message: 'Registered Successfully' });
        } catch (error) {
            reject({ message: 'Registration Failed' });
        }
    });
};

// Function to handle user login
module.exports.userLogin = async (req, res) => {
    try {
        const { email, password } = req.body;

        // Find the user by their email
        const user = await User.findOne({ email });

        if (!user) {
            // User with the provided email doesn't exist
            return res.status(401).send({ message: "Login failed, invalid email" });
        }

        // Check if the provided password matches the user's hashed password
        const isPasswordValid = await bcrypt.compare(password, user.password);

        if (!isPasswordValid) {
            // Password is invalid
            return res.status(401).send({ message: "Login failed, invalid password" });
        }

        // If email and password are valid, generate a JWT token and send it in the response
        const token = auth.createAccessToken(user); // Use your token creation logic
        return res.send({ token });
    } catch (error) {
        console.log(error);
        return res.status(500).send({ message: "Internal server error" });
    }
};

// Function to handle user checkout
module.exports.userCheckout = async (req, res) => {
    if (req.user.isAdmin) {
        // If the user is an admin, forbid the action
        return res.send({ message: "Action Forbidden" });
    }

    try {
        // Find the product by productId
        const product = await Product.findById(req.body.productId);

        if (!product) {
            // If the product is not found, return a 404 error
            return res.send({ message: "Product not found" });
        };

        // Ensure that quantity is a valid number
        const quantity = parseInt(req.body.quantity);

        if (isNaN(quantity) || quantity <= 0) {
            // If the quantity is invalid, return a 400 error
            return res.send({ message: "Invalid quantity" });
        };

        // Calculate totalAmount based on quantity and productPrice
        const totalAmount = quantity * product.price;

        // Create an order object with product details
        const orderedProduct = {
            productId: product._id,
            productName: product.name,
            quantity: quantity,
            totalAmount: totalAmount,
        };

        // Update the user's orderedProduct array with the new order
        const user = await User.findById(req.user.id);
        if (!user) {
            // If the user is not found, return a 404 error
            return res.send({ message: "User not found" });
        }

        // Push the orderedProduct object into the orderedProduct array
        user.orderedProduct.push(orderedProduct);

        // Populate the products array inside the orderedProduct array
        user.orderedProduct.forEach((order) => {
            order.products.push({
                productId: product._id,
                productName: product.name,
                quantity: quantity,
            });
        });

        await user.save();

        // Update the product's userOrders array with the user's order
        const userOrder = {
            userId: req.user.id,
            orderId: user.orderedProduct.length > 0 ? user.orderedProduct[user.orderedProduct.length - 1]._id : null,
        };
        product.userOrders.push(userOrder);
        await product.save();

        // Respond with success message
        return res.send({ message: "Order created successfully" });
    } catch (error) {
        console.log(error);
        // Handle any errors that occur during the database operations
        return res.send({ message: "Internal server error" });
    };
};



// Function to retrieve user profile
module.exports.userProfile = (req, res) => {
    // Find a user by their ID in the database and send the result
    return User.findById(req.user.id).then(result => {
        return res.send(result);
    }).catch(err => res.send(err)); // Catch and return any errors
};

// Controller method to set a user as an admin
module.exports.setUserAsAdmin = async (req, res) => {
    try {
        const { userId } = req.params;

        // Find the user by ID
        const user = await User.findById(userId);

        if (!user) {
            return res.send({ message: "User not found" });
        }

        // Set the user as an admin
        user.isAdmin = true;
        await user.save();

        return res.send({ message: "User is now an admin" });
    } catch (error) {
        return res.send({ message: "Internal server error" });
    }
};

// Controller method to retrieve authenticated user's orders
module.exports.getUserOrders = async (req, res) => {
    try {
        const userId = req.user.id;

        // Find the user by ID
        const user = await User.findById(userId);

        if (!user) {
            return res.send({ message: "User not found" });
        }

        // Retrieve the orders for the user
        const userOrders = user.orderedProduct;

        return res.send(userOrders);
    } catch (error) {
        return res.send({ message: "Internal server error" });
    }
};

// Controller method to retrieve all orders (admin only)
module.exports.getAllOrders = async (req, res) => {
    try {
        // Check if the authenticated user is an admin
        if (!req.user.isAdmin) {
            return res.send({ message: "Action Forbidden. Only admins can access this resource." });
        }

        // Retrieve all orders from all users
        const allOrders = await User.find({}, { orderedProduct: 1 });

        return res.send(allOrders);
    } catch (error) {
        return res.send({ message: "Internal server error" });
    }
};

// Controller method to add a product to the cart
module.exports.addToCart = async (req, res) => {
    try {
        const { productId, quantity } = req.body;

        if (!productId || !quantity || quantity <= 0) {
            return res.send({ message: "Invalid request" });
        }

        const product = await Product.findById(productId);

        if (!product) {
            return res.send({ message: "Product not found" });
        }

        const user = await User.findById(req.user.id);

        if (!user) {
            return res.send({ message: "User not found" });
        }

        // Calculate productPrice based on the product's price
        const productPrice = product.price;

        // Calculate totalAmount based on quantity and productPrice
        const totalAmount = quantity * productPrice;

        // Check if the product is already in the user's cart
        const existingCartItemIndex = user.cart.findIndex((item) => item.product.toString() === productId);

        if (existingCartItemIndex !== -1) {
            // If the product already exists in the cart, update its quantity, productPrice, and calculate the new subtotal
            const existingCartItem = user.cart[existingCartItemIndex];
            existingCartItem.quantity += quantity;
            existingCartItem.productPrice = productPrice;
            existingCartItem.totalAmount = calculateSubtotal(existingCartItem.quantity, productPrice); // Calculate the new subtotal
        } else {
            // Otherwise, add a new item to the cart with the calculated subtotal
            user.cart.push({ product: productId, quantity, productPrice, totalAmount });
        }

        await user.save();

        return res.send({ message: "Product added to cart successfully" });
    } catch (error) {
        console.log(error);
        return res.send({ message: "Internal server error" });
    }
};

// Controller method to change the quantity of a product in the cart
module.exports.changeCartItemQuantity = async (req, res) => {
    try {
        const { productId, quantity } = req.body;

        if (!productId || !quantity || quantity <= 0) {
            return res.send({ message: "Invalid request" });
        }

        const user = await User.findById(req.user.id);

        if (!user) {
            return res.send({ message: "User not found" });
        }

        // Check if the product is in the user's cart
        const existingCartItemIndex = user.cart.findIndex((item) => item.product.toString() === productId);

        if (existingCartItemIndex !== -1) {
            // If the product exists in the cart, update its quantity and totalAmount
            const existingCartItem = user.cart[existingCartItemIndex];
            existingCartItem.quantity = quantity;
            existingCartItem.totalAmount = quantity * existingCartItem.productPrice;

            await user.save();

            return res.send({ message: "Cart item quantity updated successfully" });
        } else {
            return res.send({ message: "Cart item not found" });
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "Internal server error" });
    }
};

// Controller method to remove a product from the cart
module.exports.removeCartItem = async (req, res) => {
    try {
        const { productId } = req.params;
        const user = await User.findById(req.user.id);

        if (!user) {
            return res.send({ message: "User not found" });
        }

        // Find the index of the cart item with the matching productId
        const cartItemIndex = user.cart.findIndex(item => item.product.toString() === productId);

        if (cartItemIndex === -1) {
            return res.send({ message: "Cart item not found" });
        }

        // Remove the cart item from the user's cart array
        user.cart.splice(cartItemIndex, 1);

        await user.save();

        return res.send({ message: "Cart item removed successfully" });
    } catch (error) {
        console.log(error);
        return res.send({ message: "Internal server error" });
    }
};

// Utility function to calculate the subtotal for a cart item
const calculateSubtotal = (quantity, productPrice) => {
    return quantity * productPrice;
};

// ...

// Controller method to get a user's cart with subtotals
module.exports.getUserCartWithSubtotals = async (req, res) => {
    try {
        // Find the user by their ID and populate the 'cart.product' field to get product details
        const user = await User.findById(req.user.id).populate('cart.product');

        if (!user) {
            return res.status(404).json({ message: "User not found" });
        }

        // Calculate subtotals for each item in the cart
        const cartWithSubtotals = user.cart.map((item) => {
            // Calculate the subtotal for each item (quantity * product price)
            const subtotal = item.quantity * item.product.price;

            return {
                product: item.product,  // Include product details
                quantity: item.quantity, // Include quantity
                subtotal: subtotal,      // Include subtotal
            };
        });

        // Send the cart with subtotals as a JSON response (without 'totalAmount')
        return res.status(200).json(cartWithSubtotals);
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "Internal server error" });
    }
};

// Utility function to calculate the total price for all items in the cart
const calculateTotalPrice = (cart) => {
    let totalPrice = 0;
    for (const item of cart) {
        totalPrice += item.totalAmount;
    }
    return totalPrice;
};

// Controller method to retrieve the total price of all items in the cart
module.exports.getTotalPriceOfCart = async (req, res) => {
    try {
        const user = await User.findById(req.user.id);

        if (!user) {
            return res.send({ message: "User not found" });
        }

        // Calculate the total price for all items in the cart
        const totalPrice = calculateTotalPrice(user.cart);

        return res.send({ totalPrice });
    } catch (error) {
        console.log(error);
        return res.send({ message: "Internal server error" });
    }
};
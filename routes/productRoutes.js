const express = require("express"); // Import the Express framework
const productController = require("../controllers/productController"); // Import the product controller
const auth = require("../auth"); // Import the authentication middleware
const router = express.Router(); // Create an Express Router instance
const { verify, verifyAdmin } = auth; // Destructure the verification functions from the authentication middleware

// Create a new product
router.post("/", verify, verifyAdmin, productController.createProduct);

// Get all products
router.get("/all", productController.getAllProducts);

// Get all active products
router.get("/", productController.getAllActive);

// Get a specific product by ID
router.get("/:productId", productController.getProduct);

// Update a specific product by ID
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

// Archive a specific product by ID
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

// Activate a specific product by ID
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);

module.exports = router;
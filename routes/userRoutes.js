// Import required packages and modules
const express = require("express"); // Express.js for building routes
const userController = require("../controllers/userController"); // User controller for handling user-related logic
const auth = require("../auth"); // Authentication middleware
const { verify, verifyAdmin } = auth; // Authorization middleware
const router = express.Router(); // Create an Express router for user routes

// Route for user registration
router.post("/register", (req, res) => {
    // Call the registerUser function from the userController
    userController.registerUser(req.body)
        .then(() => {
            res.status(200).json({ message: 'Registered Successfully' });
        })
        .catch((error) => {
            res.status(400).json(error); // Send the error messages as a JSON response
        });
});

// Route for user login
router.post("/login", userController.userLogin);

// Route for user checkout (requires authentication)
router.post('/checkout', verify, userController.userCheckout);

// Route for getting user details (requires authentication)
router.get("/details", verify, userController.userProfile);

// Create a new route to set a user as an admin
router.put("/admin/:userId", verify, verifyAdmin, userController.setUserAsAdmin);

// Create a new route to retrieve authenticated user's orders
router.get("/orders", verify, userController.getUserOrders);

// Create a new route to retrieve all orders (admin only)
router.get("/all-orders", verify, verifyAdmin, userController.getAllOrders);

// Add a product to the cart
router.post("/add-to-cart", verify, userController.addToCart);

// Change the quantity of a product in the cart
router.put("/cart/quantity/:productId", verify, userController.changeCartItemQuantity);

// Remove a product from the cart
router.delete("/cart/remove/:productId", verify, userController.removeCartItem);

// Route for getting the user's cart with subtotals
router.get("/cart", verify, userController.getUserCartWithSubtotals);

// Route for getting the total price of all items in the cart
router.get("/cart/totalPrice", verify, userController.getTotalPriceOfCart);

module.exports = router; // Export the router for use in the main application